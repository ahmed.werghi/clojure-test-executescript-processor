(import 'org.apache.commons.io.IOUtils)
(import 'java.nio.charset.StandardCharsets)
;
; HELPERS
; IOUtils.toString(inputStream,StandardCharsets.UTF_8)
; org.apache.nifi.controller.repository.io.FlowFileAccessInputStream 

(defn get-data [inputStream]
  (IOUtils/toString inputStream (StandardCharsets/UTF_8)))

(defn output-stream-callback [flow]
  (reify StreamCallback
    (process [this inputStream outputStream]
      (.write outputStream 
        (.getBytes (str "Custom-processor-clojure�� -> " (slurp inputStream)
                        ; (get-data inputStream)))))))
                        ))))))

(defn put-sample-content[flow]
  (.write session flow (output-stream-callback flow)))

(defn success-transfer [flow]
  (.transfer session flow REL_SUCCESS))

;
; MAIN
;
(let [flowFile (.get session)]
  (-> flowFile
      put-sample-content
      success-transfer))
